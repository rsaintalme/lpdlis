describe("LOCAL STORAGE", function() {

    it("#saveFile et getFile", function() {
        var laFile = new Playlist("Ma playlist", null, false, false);
        saveFile(laFile);
        expect(getFile()).toEqual(laFile);
    });

    it("#savePlaylist getPlaylist", function() {
        var playlist = new Playlist("Ma playlist", null, false, false);
        savePlaylist(playlist);
        expect(getPlaylist(playlist.getName())).toEqual(playlist);
    });
});