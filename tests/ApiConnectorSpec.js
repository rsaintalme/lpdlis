describe("Api Connector", function() {
    it("initialize Api Connector", function() {
        var connector = ApiConnector.getInstance();
        expect(connector).toBeDefined();
    });

    it("getTrackInfos", function () {
      var connector = ApiConnector.getInstance();
      expect(connector.getTrackInfos(0)).toEqual(jasmine.objectContaining({
        id: 1
      }));
    });
});
