class Track {

    /**
     * Constructeur de l'objet Track
     * Initialise un objet track avec son identifiant et appelle la méthode hydrateFromAPI()
     * permettant de finir la construction de l'objet via les informations de l'API Connector
     * @param idTrack identifiant de la piste à récupérer
     */
    constructor(idTrack) {
        this.id = idTrack;
        this.hydrateFromApi();
    }

    //Constructeur de test
    // constructor(props) {
    //     this.id = props.id;
    //     this.titre = props.titre;
    //     this.artiste = props.artiste;
    //     this.duree = props.duree;
    //     this.urlAudio = props.urlAudio;
    //     this.urlVisuel = props.urlVisuel;
    //     this.urlPartage = props.urlPartage;
    //     this.dureeEcoutee= props.dureeEcoutee;
    //     this.read = props.read;
    //     this.liked = props.liked;
    //     this.validUntil = props.validUntil;
    // }

    /**
     * Fonction qui permet de retourner un objet Track
     * S'il n'existe pas déjà dans la liste il est créé puis insérer dans la liste.
     * @param idTrack
     */
    static create(idTrack) {
        var track = null;

        if (idTrack !== null && !isNaN(parseInt(idTrack)) && idTrack !== "0") {
            // On recherche si track est déjà instanciée
            this.tracks.forEach(function(exitingTrack) {
                if (exitingTrack.id === idTrack) track = exitingTrack;
            });
            // Sinon on la créé et l'ajoute au tableau
            if (track == null) {
                track = new Track(parseInt(idTrack));
                this.tracks.push(track);
            }
        }

        return track;
    }

    /**
     * Retourne l'id du Track
     */
    getId() {
        return this.id;
    }

    /**
     * Retourne le titre
     */
    getTitre() {
        return this.titre;
    }

    /**
     * Retourne l'artiste
     */
    getArtiste() {
        return this.artiste;
    }

    /**
     * Retourne la durée sous forme de float
     */
    getDuree() {
        return this.duree;
    }

    /**
     * Retourne l'url audio
     */
    getUrlAudio() {
        return this.urlAudio;
    }

    /**
     * Retourne l'url du visuel de l'objet Track
     */
    getUrlVisuel() {
        return this.urlVisuel;
    }

    /**
     * Retourne l'url de partage de l'objet Track
     */
    getUrlPartage() {
        return this.urlPartage;
    }

    /**
     * Retourne la durée écoutée sous forme de float
     */
    getDureeEcoute() {
        return this.dureeEcoutee;
    }

    /**
     * Retourne la valeur like sous forme de booléen
     */
    isLiked() {
        return this.liked;
    }

    /**
     * Modifie l'attribut like du Track
     * @param isLiked booléen
     */
    setLike(isLiked) {
        this.liked = isLiked;
        if (isLiked) ApiConnector.getInstance().like(this.id);
        else ApiConnector.getInstance().unlike(this.id);
    }

    /**
     * Ajoute la durée déjà écoutée en secondes
     * @param sec
     */
    addDureeEcoutee(sec) {
        this.dureeEcoutee += sec;
    }

    /**
     * Récupération des informations du Track à l'API Connector
     */
    hydrateFromApi() {
        var currentInstance = this;
        //Récupération du JSON
        ApiConnector.getInstance().getTrackInfos(currentInstance.id, function(props) {

            //Remplissage des informations du track
            currentInstance.id = props.id;
            currentInstance.titre = props.titre;
            currentInstance.artiste = props.artiste;
            currentInstance.duree = currentInstance.dureeFromStringToFloat(props.duree);
            currentInstance.urlAudio = props.urlAudio;
            currentInstance.urlVisuel = props.urlVisuel;
            currentInstance.urlPartage = props.urlPartage;

            //Remplissage des informations relatives à l'utilisateur
            currentInstance.dureeEcoutee= currentInstance.dureeFromStringToFloat(props.dureeEcoutee);
            currentInstance.read = (props.read === "true" || props.read === "1" || props.read === true || props.read === 1);
            currentInstance.liked = (props.liked === "true" || props.liked === "1" || props.liked === true || props.liked === 1);
            currentInstance.validUntil = new Date(props.validUntil * 1000);
        });
    }

    /**
     * Conversion d'une durée (string) en durée (float)
     * @param dureeString la durée en format string
     */
    dureeFromStringToFloat(dureeString) {
        var dureeFloat = 0.0;
        if (dureeString != null) {
            var regex = /[0-9]{2}/g;
            var values = dureeString.match(regex);
            if (!isNaN(parseInt(values[0])) && !isNaN(parseInt(values[1])) && !isNaN(parseInt(values[2])) && !isNaN(parseFloat(values[3]))) {
                dureeFloat = parseInt(values[0]) * 3600;
                dureeFloat += parseInt(values[1]) * 60;
                dureeFloat += parseFloat(values[2] + "." + values[3]);
            }
        }
        return dureeFloat;
    }
}

Track.tracks = [];