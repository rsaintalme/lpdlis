/**
 * Classe de la pré-écoute
 * @author Aurélien, Emilie, William, Florian
 */

 // Appels des scripts pour avoir accès aux fonctions
 // nécessaires à la gestion du player et de la playlist
var player = new Player();
var dropdown = new Dropdown();
var audio = document.createElement('audio');

// Fonction qui se lance après le chargement du DOM
// à l'aide de la fonction document.addEventListener("DOMContentLoaded", bindEvents);
document.addEventListener("DOMContentLoaded", createDOM);

function createDOM(){

  var db = ApiConnector.getInstance();

  db.getSample(function(listMusique){
      affichage(listMusique);
      bindEvents();
  });

  db.getListArtist(function( listArtist){
      affichageArtiste(listArtist);
      bindEvents();
  });

}

function affichageArtiste(jsonTab){
  var div_playlist = document.querySelector('.playlist-a-l-affiche');
  var div_container = document.createElement("div");
  div_container.classList.add("music_container");

  for (var i = 0; i < jsonTab.length; i++) {
    var div = document.createElement("div");
    div.classList.add("cover_image_container");
    div.setAttribute("data-id",jsonTab[i]["id"]);
    var div_img = document.createElement("div");
    div_img.classList.add("cover_image_div");
    var img = document.createElement("img");
    img.classList.add("cover_image");
    img.src=jsonTab[i]["serveur_visuel"];
    var div_button = document.createElement("div");
    div_button.classList.add("cover_image_buttons");
    var div_button_play= document.createElement("div");
    div_button_play.classList.add("cover_image_play_container");
    var input_play = document.createElement("input");
    input_play.classList.add("cover_image_play");
    input_play.src="../images/play.png";
    input_play.type="image";
    var svg = document.createElement("svg");
    svg.classList.add("svg_circle");
    var circle = document.createElement("div");
    circle.innerHTML = "<div id='loading'><div class='hold left'><div class='fill'></div></div><div class='hold right'><div class='fill'></div></div></div>";

    var div_dropdown = document.createElement("div");
    div_dropdown.classList.add("dropdown_button_container");
    var dropdown_button = document.createElement("button");
    dropdown_button.classList.add("dropdown_button");
    var iElem = document.createElement("i");
    iElem.classList.add("fas");
    iElem.classList.add("fa-ellipsis-v");
    dropdown_button.appendChild(iElem);
    div_dropdown.appendChild(dropdown_button);

    svg.appendChild(circle);
    div_button_play.appendChild(input_play);
    div_button_play.appendChild(svg);
    div_button.appendChild(div_button_play);
    div_button.appendChild(div_dropdown);
    div_img.appendChild(img);
    div.appendChild(div_img);
    div.appendChild(div_button);
    div_container.appendChild(div);
    div_playlist.appendChild(div_container);
  }

  var html_more = document.createElement("div");
  html_more.classList.add("more_music");
  html_more.innerHTML = '<div class="more_music_bckg"><a href="#"><img src="../images/more.png" class="more-img" alt="Plus de musiques"><p class="more-text">Plus de musiques</p></a></div>';

  div_container.appendChild(html_more);

}

function affichage(jsonTab){
  var div_parent = document.querySelector('.bloc-a-l-affiche');
  var div_container = document.createElement("div");
  div_container.classList.add("music_container");

  for (var i = 0; i < jsonTab.length; i++) {
    var div = document.createElement("div");
    div.classList.add("cover_image_container");
    div.setAttribute("data-id",jsonTab[i]["id"]);
    var div_img = document.createElement("div");
    div_img.classList.add("cover_image_div");
    var img = document.createElement("img");
    img.classList.add("cover_image");
    img.src=jsonTab[i]["serveur_visuel"];
    var div_button = document.createElement("div");
    div_button.classList.add("cover_image_buttons");
    var div_button_play= document.createElement("div");
    div_button_play.classList.add("cover_image_play_container");
    var input_play = document.createElement("input");
    input_play.classList.add("cover_image_play");
    input_play.src="../images/play.png";
    input_play.type="image";
    var svg = document.createElement("svg");
    svg.classList.add("svg_circle");
    var circle = document.createElement("div");
    circle.innerHTML = "<div id='loading'><div class='hold left'><div class='fill'></div></div><div class='hold right'><div class='fill'></div></div></div>";

    var div_dropdown = document.createElement("div");
    div_dropdown.classList.add("dropdown_button_container");
    var dropdown_button = document.createElement("button");
    dropdown_button.classList.add("dropdown_button");
    var iElem = document.createElement("i");
    iElem.classList.add("fas");
    iElem.classList.add("fa-ellipsis-v");
    dropdown_button.appendChild(iElem);
    div_dropdown.appendChild(dropdown_button);

    svg.appendChild(circle);
    div_button_play.appendChild(input_play);
    div_button_play.appendChild(svg);
    div_button.appendChild(div_button_play);
    div_button.appendChild(div_dropdown);
    div_img.appendChild(img);
    div.appendChild(div_img);
    div.appendChild(div_button);
    div_container.appendChild(div);
    div_parent.appendChild(div_container);
  }

  var html_more = document.createElement("div");
  html_more.classList.add("more_music");
  html_more.innerHTML = '<div class="more_music_bckg"><a href="#"><img src="../images/more.png" class="more-img" alt="Plus de musiques"><p class="more-text">Plus de musiques</p></a></div>';

  div_container.appendChild(html_more);


}

var bindEvents = function(){

  // Récupération des boutons du dropdown du DOM
  var buttons = document.querySelectorAll('.dropdown_button_container');
  for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', function(e){
      dropdown.toggleDropdown(e.target);
    });
  }

  // Récupération des éléments play du DOM
  // Au survol de l'élément l'animation débute
  var plays = document.querySelectorAll(".cover_image_play_container");
  var tracks = document.querySelectorAll(".cover_image_container");
  for (i = 0; i < tracks.length; i++) {
    tracks[i]= Track.create(tracks[i].dataset.id);
  }

  for (i = 0; i < plays.length; i++) {
    plays[i].addEventListener('mouseover', function(e) {
      var svg = e.target.querySelector('.svg_circle');
      prePlay(e.target);

      setTimeout(()=> {
        audio.pause();
        audio.currentTime = 0;
      }, 30000);

    });

    plays[i].addEventListener('mouseout', function(e) {
      stop(e.target);
    });
  }

}



// Fonction qui contruit l'audio, l'ajoute au DOM
// et le lance automatiquement
function prePlay(Obj){
  audio.src = '../track/1.mp3';
  audio.className = 'audio';
  audio.autobuffer = true;
  audio.controls = true;
  audio.autoplay = true;

  Obj.append(audio);
  player.pause();
}

// Fonction qui met en pause l'audio et remet
// le temps à zéro
function stop(Obj){
  audio.pause();
  audio.currentTime = 0;
  player.play();

}

function play(Obj){
  Obj.play();
}
