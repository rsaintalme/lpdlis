class Dropdown {

  constructor () {
    // Data loaded to display element in dropdown
    this.data = [
      {
        'logo': "../images/playlist-plus.svg",
        'text': 'Ecouter juste après',
        'link': ''
      },
      {
        'logo': '../images/heart.svg',
        'text': 'Ajouter aux coups de coeurs',
        'link': 'dropdown.toggleLike(this)'
      },
      {
        'logo': '../images/share.svg',
        'text': 'Partager',
        'link': ''
      },
      {
        'logo': '../images/dontRecommand.svg',
        'text': 'Ne pas me recommander ce titre',
        'link': ''
      },
      {
        'logo': '../images/delete.svg',
        'text': "Retirer de la file d'attente",
        'link': ''
      }
    ];
    // Boolean true dropdown is displayed on the user's screen
    this.isOpen = false;
    this.track = null;
  }

  // Show or hide the dropdown
  toggleDropdown (button) {
    // Keep the button as a reference
    this.button = button;
    // if dropdown is not displayed
    if (!this.isOpen) {
      // Setting the boolean to true
      this.isOpen = true;
      // Create the div for the dropdown
      this.createDiv();
      // We keep this as a variable
      var self = this;
      // When the user click somewhere in the document, do something
      document.addEventListener('click', function (e) {
        // If dropdown is displayed and cursor nor on the dropdown nor the dropdown button
        if (self.isOpen && !document.getElementById('myDropdown').contains(e.target) && !self.button.contains(e.target)){
          // Delete the dropdown
          document.getElementById('myDropdown').remove();
          // Set the boolean to false
          self.isOpen = false;
        }
        // false as parameter here to be able to use the self variable
      }, false);
    // If dropdown is displayed
    } else {
      // Delete the dropdown
      this.removeDiv();
      // Set the boolean to false
      this.isOpen = false;
    }
  }

  // Initialize the div for the dropdown
  createDiv () {
    // Create the div
    var myDropdown = document.createElement('div');
    // Set the div id to 'myDropdown'
    myDropdown.id = 'myDropdown';
    // Add the class 'dropdown-content' to the div
    myDropdown.classList.add('dropdown-content');
    // add the dropdown after the dropdown button
    this.button.after(myDropdown);
    // Initialize the dropdown content list
    this.initList(myDropdown);
  }

  removeDiv () {
    // Delete the dropdown div
    document.getElementById('myDropdown').remove();
  }

  // Initialize the content list of the dropdown
  initList (myDropdown) {
    // For each index in the data array
    this.data.forEach(function (element) {
      // We create a 'a' element
      var line = document.createElement('a');
      // We set the onclick attribute to the link key in the data
      line.setAttribute('onclick', element.link);
      // We initialize an image to stock element's logo
      var img = new Image();
      // Set the src of the image to the logo key in the data
      img.src = element.logo;
      // Set the style of the image
      img.style = "position: relative; top: 6px; padding-right: 16px;";
      // Add the image to the a link
      line.appendChild(img);
      // Add span element to display text
      var text = document.createElement('span');
      // Text to display according to the text key in the data
      text.innerHTML = element.text;
      // Add the text to the a tag
      line.appendChild(text);
      // Add the a element in the dropdown
      myDropdown.appendChild(line);

      if(element.text === 'Partager') {
        line.addEventListener("click", function(e) {
          e.preventDefault();
          this.sharePopUp();
        });
      }
    });
  }

  toggleLike (object) {
    var textReference = object.querySelector('span');
    var imgReference = object.querySelector('img');
    if (imgReference.src.endsWith('heart.svg')) {
      imgReference.src = '../images/heart-plain.svg';
      textReference.innerHTML = 'Retirer des coups de coeurs';
    } else {
      imgReference.src = "../images/heart.svg";
      textReference.innerHTML = 'Ajouter aux coups de coeurs';
    };
  }

  /**
   * Ouvre une popUp avec le lien du morceau à partager
   */
  sharePopUp() {
    window.alert("Url de partage: " + this.track.getUrlPartage());
  }

  /**
  * @param track
  * associate each track object with its dropdown
  */
  setTrack(track) {
    this.track = track;
  }
}
// Initialize the object in order to be used elsewhere
var dropdown = new Dropdown();
