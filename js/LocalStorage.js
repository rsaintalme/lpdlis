/**
 * https://developer.mozilla.org/fr/docs/Web/API/Storage/LocalStorage
 * Cet algorithme est une imitation exacte de l’objet localStorage, mais utilise les cookies.
 */
if (!window.localStorage) {
    Object.defineProperty(window, "localStorage", new (function () {
      var aKeys = [], oStorage = {};
      Object.defineProperty(oStorage, "getItem", {
        value: function (sKey) { return sKey ? this[sKey] : null; },
        writable: false,
        configurable: false,
        enumerable: false
      });
      Object.defineProperty(oStorage, "key", {
        value: function (nKeyId) { return aKeys[nKeyId]; },
        writable: false,
        configurable: false,
        enumerable: false
      });
      Object.defineProperty(oStorage, "setItem", {
        value: function (sKey, sValue) {
          if(!sKey) { return; }
          document.cookie = escape(sKey) + "=" + escape(sValue) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
        },
        writable: false,
        configurable: false,
        enumerable: false
      });
      Object.defineProperty(oStorage, "length", {
        get: function () { return aKeys.length; },
        configurable: false,
        enumerable: false
      });
      Object.defineProperty(oStorage, "removeItem", {
        value: function (sKey) {
          if(!sKey) { return; }
          document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
        },
        writable: false,
        configurable: false,
        enumerable: false
      });
      this.get = function () {
        var iThisIndx;
        for (var sKey in oStorage) {
          iThisIndx = aKeys.indexOf(sKey);
          if (iThisIndx === -1) {
            oStorage.setItem(sKey, oStorage[sKey]);
          } else {
            aKeys.splice(iThisIndx, 1);
          }
          delete oStorage[sKey];
        }
        for (aKeys; aKeys.length > 0; aKeys.splice(0, 1)) { oStorage.removeItem(aKeys[0]); }
        for (var aCouple, iKey, nIdx = 0, aCouples = document.cookie.split(/\s*;\s*/); nIdx < aCouples.length; nIdx++) {
          aCouple = aCouples[nIdx].split(/\s*=\s*/);
          if (aCouple.length > 1) {
            oStorage[iKey = unescape(aCouple[0])] = unescape(aCouple[1]);
            aKeys.push(iKey);
          }
        }
        return oStorage;
      };
      this.configurable = false;
      this.enumerable = true;
    })());
  }

/*
* Ensemble de méthodes permettant d'effectuer des actions entre l'application et le local storage.
*/

//nom donné à la file d'attente
const FILE_ATTENTE = "fileAttente";

/**
 * Enregistre la file d'attente dans le local storage
 * @param {Playlist} laFile la file d'attente
 */
function saveFile(laFile) {
    localStorage.setItem(FILE_ATTENTE, JSON.stringify(laFile));
}

/**
 * Retourne la file d'attente enregistrer dans le local storage.
 * @returns {Playlist} la file d'attente
 */
function getFile() {
     //on récupère la file sous forme de JSON
     var props =  JSON.parse(localStorage.getItem(FILE_ATTENTE));
     var list = new Playlist(null, null, null, null);
     //et on reconstruit la list
     list.setName(props.name);
     list.setTracks(props.tracks);
     list.setUnreadTracks(props.unreadTracks);
     list.setCurrentTrackIndex(props.currentTrackIndex);
     list.setPrevTrackIndex(props.prevTrackIndex);
     list.setRandom(props.random);
     list.setLoop(props.loop);
     return list;
}

/**
 * Enregistre une playlist dans le local storage.
 * @param {Playlist} playlist la playlist à enregistrer.
 */
function savePlaylist(playlist) {
    localStorage.setItem(playlist.getName(), JSON.stringify(playlist));
}

/**
 * Retourne une Playlist en fonction de son nom dans le local storage
 * @param {string} nom le nom de la playlist que l'on recherche
 * @returns {Playlist} la playlist recherchée
 */
function getPlaylist(nom) {
    //on récupère la playlist sous forme de JSON
    var props =  JSON.parse(localStorage.getItem(nom));
    var playlist = new Playlist(null, null, null, null);
    //et on reconstruit la playlist
    playlist.setName(props.name);
    playlist.setTracks(props.tracks);
    playlist.setUnreadTracks(props.unreadTracks);
    playlist.setCurrentTrackIndex(props.currentTrackIndex);
    playlist.setPrevTrackIndex(props.prevTrackIndex);
    playlist.setRandom(props.random);
    playlist.setLoop(props.loop);
    return playlist;
}
