
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# profile: http://ogp.me/ns/profile#">

	<!-- Appel des nouveaux scrips et CSS -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/js/all.min.css">
	<link rel="stylesheet" href="../css/Dropdown.css">
	<link rel="stylesheet" href="../css/PrePlayer.css">
	<link rel="stylesheet" href="../css/player.css">
	<link rel="icon" type="image/png" href="../images/play.png" />

	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/js/all.min.js"></script>
	<script type="text/javascript" src="../js/ApiConnector.js"></script>
	<script type="text/javascript" src="../js/LocalStorage.js"></script>
	<script type="text/javascript" src="../js/Track.js"></script>
	<script type="text/javascript" src="../js/Playlist.js"></script>
	<script type="text/javascript" src="../js/Player.js"></script>
	<script type="text/javascript" src="../js/Dropdown.js"></script>
	<script type="text/javascript" src="../js/PrePlayer.js"></script>
    <script src="https://polyfill.io/v3/polyfill.js"></script>

	<!-- Fin appel des nouveaux scrips et CSS -->

	<link rel="stylesheet" href="../css/general.css">
	<link rel="stylesheet" href="../css/musique.css">
	<link rel="stylesheet" href="../css/pictures.css">
	<!--<base href="https://www.easyzic.com/mp3-gratuits/">-->
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" data-savepage-href="/common/gifs/16x16/favicon.gif"
		href="data:image/gif;base64,R0lGODlhEAAQAMZqABc7dhc9eBg+eBhAfBlAfBlCfxlDfxlDgBlEfxlFghlFgxpGghpGgxpIhhpJhhpLiRtLiRtLihtNjBtNjRxNjBxNjRtOjRtQkBxQkBxRkBxSlB1SkxxTkxxTlB1Tkx1TlB1Vlh1Vlx1Wlx5Wlx1YmR1Ymh5YmR5Ymh5Zmh9anR9anh5bnR9bnR9bnh5doR9doB9doR9eoB9eoR9gpCBgpCBgpSBipyBiqCBjpyBjqCBlqyFlqiFlqyBmqiFmqyFnriForSForiJoriJqsSJqsiJrsSJttCJttSNttSNutSNwuCNwuSRwuCRyuyRzuyRzvCR1viR1vyV1viV1vyR3wiV4wiZ6xSV7xSZ7xSZ7xiZ9yCZ9ySd9ySaAzCeAzCeCzyiC0CeDzyiDzyiF0iiF0ymF0ymH1iiI1imJ2P///xY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cBY3cCH5BAEKAH8ALAAAAAAQABAAAAfOgGhmZWJeXFhVUU1KSURCOzhnZGFdWllVUE9LSUVBPDk1Y2BeW1ZVU05LSI6fMzBfpGmys7Q0MCteWla0vGkyLShcprJMR0VCPDiyKigjV4myxsg2siwlIR9UirQ+NzSy1x4ZUou0ON9pJyIcGBOLR7I7NjMxsiIeGBIQSkZDsjk0XNjrgIHCgwZGivzoRQuCAwZDgPRgOMtBAgNCdOCY8SIFiRDsKkRosAABgWQ0YrAwAULDBQsQGig4MCCADRovVJwIsSGfQwUFBggAEAgAOw==">
	<meta name="Description"
		content="Ecoutez gratuitement des milliers de titres. Illimité et légal : créez vos playlists et partagez vos coups de coeur avec vos amis. MP3 rock, MP3 chanson française, MP3 blues, MP3 métal, MP3 reggae, MP3 jazz, MP3 éléctro...">
	<meta property="og:title" content="Musique / Mp3">
	<meta property="og:image" content="https://www.easyzic.com/common/skins/blhoo/gifs/menus/logo.png">
	<link rel="preload" href="/common/skins/blhoo/fonts/EasyZic.woff?1553199084" as="font" type="font/woff" crossorigin="anonymous">
	<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/common/datas/_cache/css-js/common-skins-blhoo-ie5-6.cache.css?1588082175" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" type="text/css" href="/common/datas/_cache/css-js/common-skins-blhoo-ie7.cache.css?1588082175" /><![endif]-->
	<!--[if lt IE 9]><link rel="stylesheet" type="text/css" href="/common/datas/_cache/css-js/common-skins-blhoo-ie8.cache.css?1588082175" /><![endif]-->
	<!--[if lt IE 9]><script src="/common/js/html5shim.js"></script><![endif]-->
	<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/common/datas/_cache/css-js/common-skins-blhoo-ie9.cache.css?1588082175" /><![endif]-->
	<link rel="dns-prefetch" href="//s1.easyzic.com">
	<link rel="dns-prefetch" href="//www.google-analytics.com">
	<meta property="og:site_name" content="EasyZic">
	<title>MP3 GRATUIT à écouter et télécharger - MUSIQUE GRATUITE et légale - EasyZic</title>
	<meta name="keywords" content="musique, gratuite, telecharger, mp3 gratuits, musique">
	<!--<meta property="og:url" content="https://www.easyzic.com/mp3-gratuits/">-->
	<link rel="canonical" href="https://www.easyzic.com/mp3-gratuits/">
	<script type="application/ld+json"></script>
	<link rel="preload" href="https://adservice.google.fr/adsid/integrator.js?domain=www.easyzic.com" as="script">
	<script type="text/javascript" data-savepage-src="https://adservice.google.fr/adsid/integrator.js?domain=www.easyzic.com"></script>
	<link rel="preload" href="https://adservice.google.com/adsid/integrator.js?domain=www.easyzic.com" as="script">
	<script type="text/javascript" data-savepage-src="https://adservice.google.com/adsid/integrator.js?domain=www.easyzic.com"></script>
	<script id="savepage-shadowloader" type="application/javascript">
		"use strict"
		window.addEventListener("DOMContentLoaded",
			function(event) {
				savepage_ShadowLoader(5);
			}, false);

		function savepage_ShadowLoader(c) {
			createShadowDOMs(0, document.documentElement);

			function createShadowDOMs(a, b) {
				var i;
				if (b.localName == "iframe" || b.localName == "frame") {
					if (a < c) {
						try {
							if (b.contentDocument.documentElement != null) {
								createShadowDOMs(a + 1, b.contentDocument.documentElement)
							}
						} catch (e) {}
					}
				} else {
					if (b.children.length >= 1 && b.children[0].localName == "template" && b.children[0].hasAttribute("data-savepage-shadowroot")) {
						b.attachShadow({
							mode: "open"
						}).appendChild(b.children[0].content);
						b.removeChild(b.children[0]);
						for (i = 0; i < b.shadowRoot.children.length; i++)
							if (b.shadowRoot.children[i] != null) createShadowDOMs(a, b.shadowRoot.children[i])
					}
					for (i = 0; i < b.children.length; i++)
						if (b.children[i] != null) createShadowDOMs(a, b.children[i])
				}
			}
		}
	</script>
	<!--<meta name="savepage-url" content="https://www.easyzic.com/mp3-gratuits/">-->
	<meta name="savepage-title" content="MP3 GRATUIT à écouter et télécharger - MUSIQUE GRATUITE et légale - EasyZic">
	<!--<meta name="savepage-from" content="https://www.easyzic.com/mp3-gratuits/"> -->
	<meta name="savepage-date" content="Wed Apr 29 2020 22:05:28 GMT+0200 (heure d’été d’Europe centrale)">
	<meta name="savepage-state" content="Basic Items; Retained cross-origin frames; Removed unsaved URLs; Max frame depth = 5; Max resource size = 50MB; Max resource time = 10s;">
	<meta name="savepage-version" content="17.2">
	<meta name="savepage-comments" content="">
</head>

<body class="page-index section-mp3-gratuits template-menus">
	<header id="tpl-header" role="banner">
		<div id="tpl-header-top">
			<a href="/"><span id="tpl-header-logo"></span></a>
			<form id="tpl-header-search" action="/common/index.php" method="get" role="search">
				<input type="hidden" name="file" value="recherche">
				<div><input type="text" class="text" name="q" value=""><input value="🔍" type="submit"></div>
				<label for="idauto_q9kfvx_1"><input class="radio" type="radio" name="scope" value="" id="idauto_q9kfvx_1" checked="checked"> <span>Tout le site</span></label><span style="padding-left:10px"> </span><label for="idauto_q9kfvx_2"><input
						class="radio" type="radio" name="scope" value="mp3-gratuits" id="idauto_q9kfvx_2"> <span>Musique</span></label>
			</form>
			<strong id="tpl-baseline">Déjà <span>181.699</span> membres ! Sortez de l'ombre, rejoignez-nous !</strong><br>
			<div id="tpl-header-infos1">
				<form id="tpl-header-login" action="/acces-membres/action.php?file=login" method="post"><input type="text" class="text" name="login" placeholder="Pseudo ou Email"><input type="password" class="text" name="password" value=""
						placeholder="mot de passe"><input type="hidden" name="location" value="/mp3-gratuits/"><input value="✓" type="submit"><span class="memoriser"><label><input type="checkbox" class="checkbox" value="1" name="memoriser" style="margin-left:0;"
								title="En cochant cette case, vous serez automatiquement authentifié lors de votre prochaine visite. (Cette connexion automatique est basée sur les cookies et ne sera effective que sur cet ordinateur)"> <span>Mémoriser la
								connexion</span></label></span>
					<span class="mdp-perdu">→ <a href="/acces-membres/index.php?file=password-oublie">Mot de passe perdu ?</a></span>
					<span class="inscription">→ <a href="/acces-membres/index.php?file=nouveau-compte">Inscription</a></span>
				</form>
			</div>
			<div id="tpl-header-infos2">
				<a href="/divers/presentation.html">Présentation du site</a><span style="padding:0 5px"> | </span>
				<a href="/divers/aide-index.html" rel="help">Aide générale</a><span style="padding:0 5px"> | </span>
				<a href="/divers/aide.html?topic=47341">CGU</a>
			</div>
		</div>
		<nav id="tpl-navigation">
			<div id="tpl-navigation-1">
				<ul>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class="expandable"><a href="/annuaire/"><span class="icon icon-reseau">Réseau</span></a>
						<div>
							<ul>
								<li><a href="/annuaire/groupes/">Les groupes et artistes</a></li>
								<li><a href="/annuaire/etablissements/">Les établissements musicaux</a></li>
								<li><a href="/annuaire/membres/rechercher.html">Recherche parmi les membres</a></li>
							</ul>
						</div>
					</li>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class=""><a href="/annonces-musique/"><span class="icon icon-annonce">Annonces</span></a>
					</li>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class=""><a href="/partitions-tablatures/"><span
								class="icon icon-partition">Partitions</span></a></li>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class="selected"><a href="/mp3-gratuits/"><span
								class="icon icon-musique">Musique</span></a></li>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class=""><a href="/avis-comparatifs/"><span class="icon icon-materiel">Matériel</span></a>
					</li>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class=""><a href="/dossiers/"><span class="icon icon-tuto">Cours &amp; Tutos</span></a>
					</li>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class=""><a href="/forums-musique/"><span class="icon icon-discussion">Forums</span></a>
					</li>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class=""><a href="/dates-de-concerts/"><span class="icon icon-agenda">Concerts</span></a>
					</li>
					<li onmouseover="this._className=this._className||this.className;this.className=this._className+' hover'" onmouseout="this.className=this._className" class="last right expandable"><a href="/divers/"><span>+</span></a>
						<div>
							<ul>
								<li><a href="/annuaire-musique/">Annuaire des sites web musicaux</a></li>
								<li><a href="/divers/rss.html">Flux RSS</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<div id="tpl-navigation-2">
				<ul>
					<li><a href="/mp3-gratuits/">Écouter / Télécharger</a></li>
					<li><a href="/mp3-gratuits/compil/index.html">Les Easy'Compil'</a></li>
					<li><a href="/mp3-gratuits/sessions/index.html">Les Easy'Sessions</a></li>
					<li><a href="/forums-musique/forum-easycompo,f38.html">Les Easy'Compos</a></li>
					<li><a href="/forums-musique/forum-easymagine,f41.html">Les Easy'Magine</a></li>
					<li><a href="/forums-musique/forum-easy-covers,f138.html">Les Easy'Covers</a></li>
					<li><a href="/forums-musique/forum-easy-mixbattles,f112.html">Les Easy'MixBattles</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<div id="tpl-body-container" class="menus-secondaires">
		<div id="tpl-body-menus-secondaires-container">
			<main id="tpl-body">
				<!-- debut du contenu de la page -->
				<h1>Musique / Mp3</h1>
				<h2><span>Playlist</span></h2>

				<div class="playlist-a-l-affiche">
				</div>
								
				<h2><span>Morceaux</span></h2>				
				<div class="bloc-a-l-affiche">
				</div>
				
				<p class="navigation" style="margin-bottom:2em">
					[<a href="/mp3-gratuits/index-styles.html">Tous les styles</a>]
					[<a href="/mp3-gratuits/index-artistes.html">Navigation par artistes</a>]
					<br>
					[<a title="Les derniers Mp3 ajoutés" href="classement-nouveautes.html">Nouveautés</a>]
					[<a title="Les musiques les plus populaires" href="classement-nb-telechargements.html">Les plus téléchargés</a>]
					[<a title="Les musiques les plus appréciées" href="classement-top-votes.html">Les mieux notés</a>]
				</p>

				<p class="informations" style="margin-top:2em">
					Nos artistes ont du talent ! Vous en doutez encore ?
					Vous trouverez sur cette page bon nombre de <strong>musiques à télécharger
						gratuitement</strong>. Cette musique gratuite est proposée par les artistes d'EasyZic sous licence
					Art Libre ou Creative Common :
					cela signifie que vous pouvez l'écouter librement, mais aussi télécharger les MP3 et diffuser à votre tour
					la musique que vous aimez dès lors que vous citez bien les auteurs des morceaux ainsi que la licence associée à chaque musique.
					<br>
					Alors profitez-en ! Téléchargez, et écoutez du neuf... Vos oreilles
					n'attendent que ça !
				</p>
				<div class="clearfix"></div><!-- fin du contenu de la page -->
			</main>
			<aside id="tpl-menus-secondaires-container">
				<div id="tpl-menus-secondaires" class="null">

					<div class="entite">
						<span class="titre">À la Une</span>
						<ul class="contenu">
							<li class="visuel"><a
									href="/forums-musique/20-04-theme-d-avril-reprenez-une-chanson-de-jacques-higelin,t304123.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire&utm_campaign=easycover-2020-04-Higelin&utm_medium=a-la-une&utm_source=www.easyzic.com%2Fdates-de-concerts"><span
										class="visuel" style="background-image:/*savepage-url=//s1.easyzic.com/accueil/19/_194/thumb.jpg*/var(--savepage-url-59)"></span><span>Proposez-nous votre reprise d'un morceau de Jacques Higelin</span></a></li>
							<li class="visuel"><a
									href="/forums-musique/easycompo-n-12-a-vous-de-mettre-en-musique-l-un-de-ces-deux-textes,t303677.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire&utm_campaign=easycompo-12-musiques&utm_medium=a-la-une&utm_source=www.easyzic.com%2Fdates-de-concerts"><span
										class="visuel" style="background-image:/*savepage-url=//s1.easyzic.com/accueil/20/_204/thumb.jpg*/var(--savepage-url-60)"></span><span>Proposez-nous une musique sur l'un de ces 2 textes imposés</span></a></li>
						</ul>
					</div>
					<div class="entite">
						<span class="titre">Les sessions</span>
						<ul class="contenu">
							<li class="visuel"><a href="/mp3-gratuits/sessions/index.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire"><span class="visuel"
										style="background-image:/*savepage-url=/common/skins/blhoo/gifs/menus/menus-secondaires/mp3-easysessions.jpg*/var(--savepage-url-61)"></span><span>Composez à plusieurs ! À partir d'un riff de base commun à tous les participants,
										proposez votre ligne instrumentale pour contruire un morceau.</span></a></li>
						</ul>
					</div>
					<div class="entite">
						<span class="titre">EasyPlayer ♪</span>
						<ul class="contenu">
							<li><a href="/mp3-gratuits/player.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire"><span>Ecoutez de la musique en surfant</span></a></li>
						</ul>
					</div>
					<div class="entite">
						<span class="titre">Classements</span>
						<ul class="contenu">
							<li title="Les derniers MP3 ajoutés"><a href="/mp3-gratuits/classement-nouveautes.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire"><span>Les nouveautés</span></a></li>
							<li title="Les MP3 ayant le plus fort taux de téléchargement"><a href="/mp3-gratuits/classement-taux-telechargement.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire"><span>Les plus populaires</span></a></li>
							<li title="Les MP3 les plus téléchargés chaque jour"><a href="/mp3-gratuits/classement-nb-telechargements.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire"><span>Les plus téléchargés</span></a></li>
							<li title="Les MP3 les mieux notés par les internautes"><a href="/mp3-gratuits/classement-top-votes.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire"><span>Les plus appréciés</span></a></li>
							<li title="Les MP3 les mieux notés par les internautes"><a href="/mp3-gratuits/classement-oublies.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire"><span>Les oubliés</span></a></li>
						</ul>
					</div>
					<div class="entite">
						<span class="titre">Les compil'</span>
						<ul class="contenu">
							<li class="visuel"><a href="/mp3-gratuits/compil/index.html?utm_campaign=global&utm_medium=internal-link&utm_source=menu-secondaire"><span class="visuel"
										style="background-image:/*savepage-url=/common/skins/blhoo/gifs/menus/menus-secondaires/mp3-easycompil.jpg*/var(--savepage-url-62)"></span><span>Les meilleurs morceaux proposés sur EasyZic ont été regroupés au sein de compilations
										téléchargeables.</span></a></li>
						</ul>
					</div>
				</div>
			</aside>
		</div>
	</div>
	<footer id="tpl-footer">
		<div id="tpl-footer-container">
			<div id="tpl-footer-links">
				<ul>
					<li><span>Aidez EasyZic !</span>
						<div>
							<ul>
								<li><a href="/common/index.php?file=paiement%2Fdon">Faire un don</a></li>
								<li><a href="/divers/pub/inviter.html">Conseiller EasyZic à un ami</a></li>
								<li><a href="/divers/pub/promouvoir.html#onglet-lien">Faire un lien</a> / <a href="/divers/pub/promouvoir.html#onglet-flyers">Affiches &amp; flyers</a></li>
							</ul>
						</div>
					</li>
					<li><span>Partenariats</span>
						<div>
							<ul>
								<li><a href="/divers/pub/partenariats.html">Types de partenariats</a></li>
								<li><a href="/divers/pub/partenaires.html">Les sites partenaires</a></li>
							</ul>
						</div>
					</li>
					<li><span>Support</span>
						<div>
							<ul>
								<li><a href="/forums-musique/forum-a-propos-d-easyzic,f80.html">Questions / suggestions...</a></li>
								<li><a href="/divers/contact.html">Contacter EasyZic</a></li>
							</ul>
						</div>
					</li>
					<li id="tpl-partenaires">
						<a title="InfoGroupe" target="_blank" rel="noopener" href="http://www.info-groupe.com/"> <img data-savepage-src="/common/gifs/partenaires/InfoGroupe.gif"
								src="data:image/gif;base64,R0lGODlhWAAfAPcAAPHLie3EheqyVf2JA/2EAfvx4v2lA+ynHeyjO+2zSO7DfPXiy/+5EuynKOuWUu2zO/LDU/LLpeelQ/bexvPHmf9+AP+0Af3FAeiqV/SqAvPcveq3h//+/P3JAv769fno1Oy6bf3SBP62AfXiw/66C/LUnfyaA/ru4vPcs/358v78+fLWtP2RBPCtKPnt3f2HE/WmBfm+Af6yAf+IEemVKP6RE/OzDfG0ffjj0u7Sq/jlyv3FBP3BBPWqC/G0Ne/TmfN7F/Tare6+cvbm1Py4AfbgvP61BOyELu+5Rvq5Af65AfC8ae7NmvG5KPy1AeSbOPm0Aeu3cOy5Yu7Usv3IBO7NkvvCAvSlC/67AvKqHvz27fjs2Pbp0/68AfLWqf66Aey+fP68AvOwE/69A/25BOqJPe25Mf3NAvy8Ae+rF/mwBv+2Av60A/jBF/+4AvmtAv3CAvu7AvixAO+sNvOnE/338P20APK6i/jp2PHPovWxBfy6AfrFBPKzHfPPs/e6D/6+A/+sAOyXCfrFAv2xBe3Gkfvw3/N8EOumePy/AP7MBv316frs2fq8BPaaHvnr3v6yAv7BAum1YPO7FPyrA+65VvXUu+S0avu/A+2+mO11FPmoAvy+AvqvBOq1bPCsduydFv++AfyzAu+jGP3BAfnmw/2sBPvz5v7AAOzHnfvAAfz17f6OEvOGI/RzAPp5Af+FDfzKBfy3A/3u2Ou5H/uxAP7CA//DAfjnz/x/Afu0BfSLCvXbpv3z5/m0Ffvz6uyNHv61CPrOA/9+Av6vA/6wAOx/Iv6nA/p+Au10B/vDBf2NFPy4A/u2BPuJEv99AvvIAv6sFfmrBf6AAf+QIee8f/zr3ffEJ/nav/bpxPa/CO6MMPXYvvl5Bvh8Cfx/B+2uQO6eWOW/YOpvDea4b/FvAPzPA/a4CPmBEvvHBveLHf2oB/7HBu2/p/iZDv779ua3cumwcemfbvvn1/SSAfGdCfqGGPu6A/HAluOhSPqgBP2wAfCiBvWBD/58AP///yH5BAAAAAAALAAAAABYAB8AAAj/AKnBmuFshjcgQLzNWLZshsOHEB++kDgxosUXzlq9ONTPXit0hzZutLfLnSN1IF+wWslqWY2XNVi9qFgRIixqMypMy5Xs05ACFJLpnEZg2rQKSJMiffYsl1OlUJUaBbKgDL4bDiIgumPMkqYI4TYAu1omArKiBAawYDGA6IABRY9GreBQpyt8//L+w3bkFYG0cqPmegs319ykRnd2w7Fh3oRMfsItQPRP3iME3BrryDThFdK3Rv/mclv08IxoBo41UKE37xBBkIjJpmSg9rHaBnTJMqWLDG3cqdfdBl47z6oTyKXUm4fnFJ4JcgAYKvBrlZBAxyjFJkaJ0j5T4DsR/xpu+/axaAxEuGnhoXVeIZwSoSEViYgIEUQShYLT4QIcOBdwwsZ9IuwRBhZYhEGELER0MYYSc3CQwwopZIGGFyrE8w8AXTShQhAjeNBAD2SMwQMWaKAxhi1d3IciIPbdR4R9RDDAgB0WdBIAB+5VQUYoXSTShRsyiDAGGm4oYcsFHcBBChYy2GGHEwjiN8YXTvCAyRgXJKEAOOAAEMMgSAAgBhhzkKGLEA8kIAQNT8hBQhhruCGKHnrEcUsXa4iABRFS2kGgCDbaIcMabxSi1ztg2BBJF2F00aITgIxBhCh22AKNFdBcoAQkMhQ5BhZfjMEJFmtoyWQkWeTBxDk+TP9RDRRnILECGG8sMcEKfljCzQo+hCKHFCsUgEceD3BCihNKiBCqBQQqYaMFFrghggA8/qNBM10AggWkWNihhHyA7IEgKhdQEQmO1K4x6hdfkOqGMgBe8AUYeSFRwj8cVKIKCv+4AAoX7v2jgBpVuJeCJCcqoYQM1FqwBrXT0iHAJbjodUoLoXSMRbMWYGFFJJH8F0McXRCBhRMR25EgEW6EoYQTtvxnLzn5BpGXIWkw8c88oyzwjwdD4DDEB0iAkJcKLpzAYwoPoAKkG1TfRzED+yha8BYKPNBDM1AQAUgH5nTxhRVWsCMkEWQ4MTEkRGCygxVorAEJfmFc+UUU+fL/olcJmfzzSND/4IHB4S3okfE7UuhhAwB5eeGEGeAgMAcC4GTBhgU2plFAwa1pQEcX7AxyRgg7AAJHDDvwsEe8Ihi6BjuYxBDLDobakeQXSnTB9z9I+JyXBx8IPsoE/OoVxAHtFfHGLVTYsMU/OhzweWsjZMB5IxGC3hoY93TQQQghxEIKHDtwAmOVFogSyaZwxEJF7NDy7obv+fqswjt6DS506KDgHwrExo4MDIF6LUiBexgBA86RYBSn8J5eVvGAM5AtBOYgBScAxAM0wOELohCFCJTBh3TsoD8Pg1aSHPY7CCRMC8IzHvJcIAQQCGEOMCieFsBBBChIgUd50EUl/6oAAABUIQDgoBgJuifBvEyAFheARgiocKA9wAEQkWjEAYQghB5YQRE84IQVYiGCzQkqDG5YQwsTlgIzZCMvjziA0BaghlAcSBQByMsvgoACBfarC7bQzy1usQYjbI4BJABHE1ujAxsoYhBo6IIVmBSKNACANS6gQS3mZoUdYEEUuqCWHYjQLAXkBQL7esc1zKCFf5zgABlbQCfIIIs1dKEHOnOPAmRhBB7YAhW38JTEHDgH1iwyLz9oRMcmyYwWBKAAKRiBmd4ACTYo4QsioJYI1rAGKUnsAR8YwQEkUQAUpOEc8HBBCdQAABfkoQVqSEISiCGNeghhAXXoxQowMP8KGMBAFFZwnA160INOOLAH0zvmP1SggANQQg1zyEEKXMABLYwCZRGjVpGstQYlTAxab8CABgzBCAkw4RSnsMYUGnCLNKTiFCrAhRQakYUFAAAKdOBCHmhQBEOcQqb8QIEhhsoIATiQDi445hBGoEcNTIEDXICAAP6xiDbwQRQZbVca65RGCxRDAO3hAAcEoAOq1uEfXuiBBvjVHoMd4B+z2EQL/jGCfCx0ERxIgSdYs4pfpEAAhGLAJvJwTE+kAQ/uGUIAkiqEWIQAEzJgA8ug1SxudlUE+sgYAFpAi0kw4h80kABdEfCPFEghDQnbAgI4UAAYzHUET/hHEAShKDD/pMAD8MDAE97AOQasIQMguJ73dPDMfxSgCkI1RB1S8AMpnkERWBgQ1YgwhjDgh5RYzYIHFqEHJKDgAVxIQTUM8Q8pmLIQkxTDCTyAARUYIgMe0oBdR5CDikpAgXnRgfZsRAj8tIBgTUxBAJqgCEycQxt9mIQ2hJGIHQiDCoK60h70owRORIITcbAAHXrhgQNADmMpiABd+bGEf0xAD6F4gAd+YVdDbMIHdBVtEZiggUqM4h0qmEIOwGBQG01sDTYQrvdwwY8z7IBFVAjBGeBwhgukoxFnIMIaEOSgBgMiCWiwRSQ2sYJ/cGEW/8AYByTwOQFAUFuL/ccKRtELDvyg/xT/4EVsUQAFUbghCzjOAwUCkIU1TMsCRuiBkL2XjRUgwQpYYJIIqDCIDuxAFB3oggXCAC8sLKm6YUgEHERwgLLmBQRlbYEQ/lGIYkhiFXpZQB+IoABjthEJ/0DBIL5ggSy0Ui9C6G27YPC/RRYACcoIwwV2QAQqKEMZT+5AKGTgBiz4iRMxkBQgtOQGI1xBAFFYgg0SIIV7/EEKPnCCKBoAAgUIQAxKIEMnHiAOSTShA74Qhw860CxTJEASIJCEALLghmk5QQSU8MIiU1GJSQAC0XBgRxJ4EIIunCjSbIDYGmQgyS6EAguc2MHEt9knX+6AFKSwFyG4uQZCuCEUcf9ghhHO0Ig+QMAMVLiAGSAwhx4ogQ6jeIMSpNGADNiIDcQYEDja6j0wrMEcO4BDHCLRyT2kAw3Q6IAiiAAJjRZDBKkjwhciwYOs2iEMtoikiezDTWf5KRFfAAQUfsCa7AEgW0XQhxA4AAYY7EgAwUhPHCCxhisM2j1cuEKSoRGLdY3PCmFQRAiE8SxtygALgAgDIGCUVWrljROc6BM2leAGNERCFR1QhSxKzAUwPKASVK1GEUgNuTqkgn9LYEMbqJCOWCThHGttYgLuYQ5zoAIKhOABH+RHvkEwgw3IN4LydcMMZvDSkMlHvhNkQX0jkCEOe7jHGBSxAz5QoQNQCML/O/rQgUjsSwio0O4CmMABxBZvCcFowxs2cY9j6IMC7vFAtvJyB2SwwBTSYAIsIIAG0AyyoA8CSBgKuIAMyIBrsRYmMByboA8DkAcqQAP0YAI38A/xgAzboAKWIGIOcANl8A7hMAC+YALrsA76oA8Y4B5+MA+tMQHloBMskBr7QAmmYAJvwRb+cBhACBWj8RdpgYADCAxAcASr0AsT8AnjYA0qwA1O4wD4tw2vsA0cEA7f4AsGYAKEYAImgABE9w/tcAetUQfGsBP+QBRgCBcQmAvDEIRyOAyJIRpqwQK7MASW4ApHgA3WcAevcAR+kAI4EA6v8Ak40ArTAAR+cASwAhAQADs="
								alt="InfoGroupe" width="88" height="31">
						</a>
					</li>
				</ul>
			</div>
			<div id="tpl-footer-mentions">
				Copyright © EasyZic 2003-2020 Tous droits réservés
				- Actuellement, 24 membres sont connectés sur le site.
			</div>
		</div>
	</footer>
	<script></script>
	<script></script>


	<a href="javascript:easyzic.skin.navigationButtons.scrollToTop();" id="btn-top" style="display: none;"></a><a href="javascript:easyzic.skin.navigationButtons.scrollToBottom();" id="btn-bottom" style="display: block;"></a><ins
		class="adsbygoogle adsbygoogle-noablate" style="display: none !important;" data-adsbygoogle-status="done"><ins id="aswift_3_expand"
			style="display:inline-table;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><ins id="aswift_3_anchor"
				style="display:block;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><iframe sandbox="" srcdoc="<html><head>
<base href=&quot;https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-1800131694705757&amp;output=html&amp;adk=1812271804&amp;adf=3025194257&amp;lmt=1588190706&amp;plat=1%3A1081352%2C2%3A1081352%2C8%3A32776%2C9%3A32776%2C16%3A8388608%2C17%3A32%2C24%3A32%2C25%3A32%2C30%3A1081344%2C32%3A32%2C40%3A32&amp;guci=1.2.0.0.2.2.0.0&amp;format=0x0&amp;url=https%3A%2F%2Fwww.easyzic.com%2Fmp3-gratuits%2F&amp;ea=0&amp;flash=21.0.0&amp;pra=7&amp;wgl=1&amp;adsid=ChAI8Muk9QUQgdGOyuLw4-ovEjsAXG_PlEks841tRjCHVkx0ieZcajv2Yq5NX3VAbLKPNINZIZlYBy-GoPyjiN5dH85vdc9DiwflQfux-Q&amp;dt=1588190706250&amp;bpp=2&amp;bdt=456&amp;idt=326&amp;shv=r20200427&amp;cbv=r20190131&amp;ptt=9&amp;saldr=aa&amp;abxe=1&amp;prev_fmts=728x90%2C300x250%2C300x250&amp;nras=1&amp;correlator=7203964253340&amp;frm=20&amp;pv=1&amp;ga_vid=571446315.1537363898&amp;ga_sid=1588190706&amp;ga_hid=1341259829&amp;ga_fc=0&amp;icsg=10474&amp;dssz=11&amp;mdo=0&amp;mso=0&amp;u_tz=120&amp;u_his=19&amp;u_java=0&amp;u_h=1080&amp;u_w=1920&amp;u_ah=1040&amp;u_aw=1920&amp;u_cd=24&amp;u_nplug=1&amp;u_nmime=2&amp;adx=-12245933&amp;ady=-12245933&amp;biw=1903&amp;bih=966&amp;scr_x=0&amp;scr_y=0&amp;eid=21065473%2C21065475%2C423550200&amp;oid=3&amp;pvsid=1034279516652575&amp;pem=45&amp;rx=0&amp;eae=2&amp;fc=896&amp;brdim=1912%2C-8%2C1912%2C-8%2C1920%2C0%2C1936%2C1056%2C1920%2C966&amp;vis=1&amp;rsz=%7C%7Cs%7C&amp;abl=NS&amp;fu=8208&amp;bc=29&amp;jar=2020-04-02-18&amp;ifi=3&amp;uci=a!3&amp;dtd=337&quot;>
<style id=&quot;savepage-cssvariables&quot;>
  :root {
  }
</style>
              </head><body></body></html>" data-savepage-crossorigin="" id="aswift_3" name="aswift_3" style="left:0;position:absolute;top:0;border:0;width:undefinedpx;height:undefinedpx;"
					data-savepage-src="https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-1800131694705757&output=html&adk=1812271804&adf=3025194257&lmt=1588190706&plat=1%3A1081352%2C2%3A1081352%2C8%3A32776%2C9%3A32776%2C16%3A8388608%2C17%3A32%2C24%3A32%2C25%3A32%2C30%3A1081344%2C32%3A32%2C40%3A32&guci=1.2.0.0.2.2.0.0&format=0x0&url=https%3A%2F%2Fwww.easyzic.com%2Fmp3-gratuits%2F&ea=0&flash=21.0.0&pra=7&wgl=1&adsid=ChAI8Muk9QUQgdGOyuLw4-ovEjsAXG_PlEks841tRjCHVkx0ieZcajv2Yq5NX3VAbLKPNINZIZlYBy-GoPyjiN5dH85vdc9DiwflQfux-Q&dt=1588190706250&bpp=2&bdt=456&idt=326&shv=r20200427&cbv=r20190131&ptt=9&saldr=aa&abxe=1&prev_fmts=728x90%2C300x250%2C300x250&nras=1&correlator=7203964253340&frm=20&pv=1&ga_vid=571446315.1537363898&ga_sid=1588190706&ga_hid=1341259829&ga_fc=0&icsg=10474&dssz=11&mdo=0&mso=0&u_tz=120&u_his=19&u_java=0&u_h=1080&u_w=1920&u_ah=1040&u_aw=1920&u_cd=24&u_nplug=1&u_nmime=2&adx=-12245933&ady=-12245933&biw=1903&bih=966&scr_x=0&scr_y=0&eid=21065473%2C21065475%2C423550200&oid=3&pvsid=1034279516652575&pem=45&rx=0&eae=2&fc=896&brdim=1912%2C-8%2C1912%2C-8%2C1920%2C0%2C1936%2C1056%2C1920%2C966&vis=1&rsz=%7C%7Cs%7C&abl=NS&fu=8208&bc=29&jar=2020-04-02-18&ifi=3&uci=a!3&dtd=337"
					src="" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!3" data-load-complete="true" data-savepage-key="0-5"
					frameborder="0"></iframe></ins></ins></ins><iframe srcdoc="<html><head>
<base href=&quot;https://www.easyzic.com/mp3-gratuits/&quot;>
<style id=&quot;savepage-cssvariables&quot;>
  :root {
  }
</style>
        </head><body></body></html>" data-savepage-sameorigin="" id="google_osd_static_frame_1555529183526" name="google_osd_static_frame" style="display: none; width: 0px; height: 0px;" data-savepage-key="0-4"></iframe>

				<div id="entirePlayer" class="entirePlayer">
				<div id="entirePlaylist" class="fileAttente">
					<div id="playlistHeader"></div>
					<div id="playlist"></div>
				</div>
					<div id="player"></div>
				</div>
</body><iframe sandbox="" srcdoc="<!DOCTYPE html><html><head>
<base href=&quot;https://googleads.g.doubleclick.net/pagead/html/r20200427/r20190131/zrt_lookup.html#&quot;>
<style id=&quot;savepage-cssvariables&quot;>
  :root {
  }
</style>
      </head><body><script></script>
</body></html>" data-savepage-crossorigin="" id="google_esf" name="google_esf" data-savepage-src="https://googleads.g.doubleclick.net/pagead/html/r20200427/r20190131/zrt_lookup.html#" src="" style="display: none;"
	data-ad-client="ca-pub-1800131694705757" data-savepage-key="0-0"></iframe>

</html>
